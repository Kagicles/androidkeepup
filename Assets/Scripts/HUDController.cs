﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {

    public Text currScore;
    public Text currCombo;
    public Text highScore;

    ///////////  Start Singleton Block  ///////////
    public static HUDController Instance;
    void Awake()
    {
        if (Instance == null)
        { Instance = this; }
        else if (Instance != this)
        { Destroy(gameObject); }
    }
    ///////////  End Singleton Block  ///////////

    void Start()
    {
        HUDController.Instance.UpdateHUD();
    }

    public void UpdateHUD()
    {
        currScore.text = GameController.Instance.currScore.ToString();
        currCombo.text = "x" + GameController.Instance.currBalls.ToString();

        if(GameController.Instance.highScore != 0)
        {
            highScore.text = GameController.Instance.highScore.ToString();
        }
        else
        {
            highScore.text = " ";
        }
    }
}