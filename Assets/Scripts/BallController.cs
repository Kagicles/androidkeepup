﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    Rigidbody2D rb;
    private Vector2 extents;

    [Header("Juggle Force")]
    [SerializeField]
    private float juggleforce;

    [Header("Max Velocity")]
    [SerializeField]
    private float maxVelocity;

    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        extents = GetComponent<PolygonCollider2D>().bounds.extents;
    }

    void Update ()
    {
        ClampToEdges();
        
    }

    void ClampToEdges()
    {
        if(transform.position.x < (GameController.Instance.leftBorder + extents.x))
        {
            transform.position = new Vector2(GameController.Instance.leftBorder + extents.x, transform.position.y);
            rb.velocity = new Vector2(-rb.velocity.x, rb.velocity.y);
        }

        if (transform.position.x > (GameController.Instance.rightBorder - extents.x))
        {
            transform.position = new Vector2(GameController.Instance.rightBorder - extents.x, transform.position.y);
            rb.velocity = new Vector2(-rb.velocity.x, rb.velocity.y);
        }

        if (transform.position.y < (GameController.Instance.botBorder - extents.y))
        {
            GameController.Instance.RemoveBall(this.gameObject);
        }
    }

    void CapVelocity()
    {
        rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxVelocity);
    }

    public void Juggle()
    {
        // Zero current velocity
        rb.velocity = new Vector2(0, 0);

        // Find vector from mouse point to gObject co-ord, nomalised
        Vector2 objPos = transform.position;
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 forceDir = (objPos - mousePos).normalized;
        Vector2 juggleDir = new Vector2(forceDir.x, 1);


        // Add juggle force in calc'd force direction
        rb.AddForce(juggleDir * juggleforce);

        // Add Score
        GameController.Instance.AddScore();

        // Add Juggles
        GameController.Instance.AddJuggle();
    }
}