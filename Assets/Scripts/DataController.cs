﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[Serializable]
public class DataController : MonoBehaviour {

    ///////////  Start Singleton Block  ///////////
    public static DataController Instance;
    void Awake()
    {
        if (Instance == null)
        { Instance = this; }
        else if (Instance != this)
        { Destroy(gameObject); }
    }
    ///////////  End Singleton Block  ///////////

    public void SaveScore(int playerScore)
    {
        //Create the serializer
        XmlSerializer serializer = new XmlSerializer(typeof(int));
        //Create the stream
        FileStream stream = new FileStream(Application.persistentDataPath + "//SavedData.xml", FileMode.Create);
        //Saves the data
        serializer.Serialize(stream, playerScore);
        //Closes the stream
        stream.Close();

        Debug.Log(Application.persistentDataPath);
    }

    public int LoadScore()
    {
        //Check if data exists
        if (File.Exists(Application.persistentDataPath + "//SavedData.xml"))
        {
            //Create the serializer
            XmlSerializer serializer = new XmlSerializer(typeof(int));
            //Locate and open the stream
            FileStream stream = new FileStream(Application.persistentDataPath + "//SavedData.xml", FileMode.Open);
            //Loads the data
            int container = (int)serializer.Deserialize(stream);
            //Closes the stream
            stream.Close();
            //Return Data
            return container;
        }
        else
        {
            Debug.Log("No Save data to load");
            return 0;
        }
    }
}