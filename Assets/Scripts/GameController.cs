﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    // Score Variables

    [Header("Current Score")]
    public int currScore;

    [Header("High Score")]
    public int highScore;

    [Header("Score Per Juggle")]
    [SerializeField]
    private int scorePerJuggle;

    // Progression Variables

    [Header("Juggles So Far")]
    [SerializeField]
    private int jugglesSoFar;

    [Header("Juggles To Add Ball")]
    [SerializeField]
    private int jugglesToAdd;

    [Header("Live Balls")]
    public int currBalls = 2;

    // Backstage Variables

    [Header("X-Bounds By Viewport")]
    public float leftBorder;
    public float rightBorder;
    public float topBorder;
    public float botBorder;

    [Header("Ball Prefab")]
    public GameObject ballPrefab;

    ///////////  Start Singleton Block  ///////////
    public static GameController Instance;
    void Awake()
    {
        if (Instance == null)
        { Instance = this; }
        else if (Instance != this)
        { Destroy(gameObject); }
    }
    ///////////  End Singleton Block  ///////////

    void Start ()
    {
        Application.targetFrameRate = 120;
        GenerateBounds();

        highScore = DataController.Instance.LoadScore();
	}

    void Update()
    {
        CheckTouch();
        CheckMouse();
    }

    // Checks touch input, then raycast to grab touched object, then runs Interact Logic
    void CheckTouch()
    {
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    // Construct a ray from the current touch coordinates
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                    RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
                    if (hit.collider != null)
                    {
                        Interact(hit.collider.gameObject);
                    }
                }
            }
        }
    }

    //Checks mouse input, then raycast to grab clicked object, then runs Interact Logic
    void CheckMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
            if (hit.collider != null)
            {
                Interact(hit.collider.gameObject);
            }
        }
    }

    void Interact(GameObject obj)
    {
        if (obj.gameObject.CompareTag("Ball"))
        {
            BallController ballScript = obj.GetComponent<BallController>();
            ballScript.Juggle();
        }
    }

    void GenerateBounds()
    {
        leftBorder  = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).x;
        rightBorder = Camera.main.ViewportToWorldPoint(new Vector2(1, 0)).x;
        topBorder   = Camera.main.ViewportToWorldPoint(new Vector2(0, 1)).y;
        botBorder   = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).y;
    }

    public void AddScore()
    {
        currScore += (currBalls * scorePerJuggle);
        HUDController.Instance.UpdateHUD();
    }

    // Add to current juggles, if enough juggles to add a ball, add the ball, reset current juggles.
    public void AddJuggle()
    {
        ++jugglesSoFar;

        if (jugglesSoFar >= jugglesToAdd)
        {
            jugglesSoFar = 0;
            AddBall();
        }
    }

    // All ball to scene, add to currBalls, update HUD
    public void AddBall()
    {
        Instantiate(ballPrefab, new Vector3(0, topBorder, 0), Quaternion.identity);
        currBalls++;
        HUDController.Instance.UpdateHUD();
    }

    // If ball falls out of bounds, destroy and remove from currBalls, if no balls left, reset level
    public void RemoveBall(GameObject obj)
    {
        Destroy(obj.gameObject);
        currBalls -= 1;

        if (currBalls == 0)
        {
            ResetLevel();
        }

        HUDController.Instance.UpdateHUD();
    } 

    // Resets current working values for run, adds new highscore if necessary, then resets balls
    public void ResetLevel()
    {
        if(currScore > highScore)
        {
            highScore = currScore;
            DataController.Instance.SaveScore(highScore);
        }

        currScore = 0;
        currBalls = 2;
        jugglesSoFar = 0;

        HUDController.Instance.UpdateHUD();

        Instantiate(ballPrefab, new Vector3(0.4f, 2.4f, 0), Quaternion.identity);
        Instantiate(ballPrefab, new Vector3(-0.4f, 1.6f, 0), Quaternion.identity);
    }
}